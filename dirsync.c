/* wejście/wyjście*/
#include <stdio.h>
#include <stdlib.h>       
#include <sys/types.h>
#include <sys/stat.h>
/* obsługa katalogów */
#include <dirent.h>
/* obsługa typu bool */
#include <stdbool.h>
/* obsługa wyjątków */
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
/* zaawansowane funkcje do pracy ze stringami */
#include <string.h>
/* sygnały */
#include <signal.h>
/* log systemowy */
#include <syslog.h>
/* pomiar czasu */
#include <time.h>
/* pełne ścieżki do plików i katalogów */
#include <linux/limits.h>
/* zmiana daty modyfikaci/stworzenia pliku */
#include <utime.h>
/* mapowanie plików do pamięci mmap */
 #include <sys/mman.h>

/* static hermetyzuje zmienną/funkcję w obrębie tego pliku */
/* czas snu daemona */
static unsigned int sleep_time = 300;
/* rekursywność */
static bool recursive = false;
/* od ilu bajtów używać mmap() zamiast open()
   gdy mniej niż 1 to opcja nieaktywna */
static unsigned int threshold = 0;
/* katalog źródłowy */
static char root_src[PATH_MAX];
/* katalog zsynchronizowany */
static char root_dst[PATH_MAX];
/* zmienna globalna przechowująca czas */
static char *current_time;

/* funkcja zwracająca obecny czas */
struct tm get_time(){
    time_t now = time(NULL);
    struct tm formated_now = *localtime(&now);
    return formated_now;
}
/* funkcja formatująca obecną datę i czas do stringa */
char *get_time_str(){
    if(current_time != NULL){
        free(current_time);
    }
    current_time = malloc(20);
    struct tm temp = get_time();
    sprintf(current_time, "%d:%d:%d %d/%d/%d",
        temp.tm_hour, temp.tm_min, temp.tm_sec, temp.tm_mday, temp.tm_mon + 1, temp.tm_year + 1900);
    return current_time;
}

/* walidacja dostarczonych argumentów */
int validate_arguments(int argc, char **argv){
    /* wyświetlenie pomocy */
    if(argc == 2 && strcmp(argv[1], "-h") == 0){
        printf("usage: dirsync [SRCDIRECTORY] [DSTDIRECTORY] [-s sleep_time] [-t threshold] [-R]\n\n" );
        printf("  Required:\n" );
        printf("  SRCDIRECTORY   source directory\n" );
        printf("  DSTDIRECTORY   destination directory\n\n" );
        printf("  Optional:\n" );
        printf("  -s             if sleep_time greater than 0, set it as sleep time of daemon in seconds\n" );
        printf("  -t             if threshold greater than 0, set it as number of bytes over which mmap instead of read will be used\n" );
        printf("  -R             turn on recursive synchronization\n" );
        exit(EXIT_SUCCESS);
    }

    /* sprawdza czy ilość argumentów nie za mała*/
    if(argc < 3){
        errno = EINVAL;
        return EXIT_FAILURE;
    }
    /* sprawdza czy ilość argumentów nie za duża*/
    if(argc > 8){
        errno = EINVAL;
        return EXIT_FAILURE;
    }

    /* Weryfikacja ścieżek do katalogów */
    /* katalog źródłowy */
    DIR *srcDir = NULL;
    if((srcDir = opendir(argv[1])) == NULL){
        return EXIT_FAILURE;
    }
    closedir(srcDir);
    /* katalog docelowy */
    DIR *dstDir = NULL;
    if((dstDir = opendir(argv[2])) == NULL){
        return EXIT_FAILURE;
    }
    closedir(dstDir);

    /* sprawdzenie argumentów dodatkowych */
    int i;
    int temp;
    for(i = 3; i < argc; i++){
        /* sprawdź czy jest opcja -R */
        if(strcmp(argv[i], "-R") == 0){
            recursive = true;
        }
        /* z jakiegoś powodu strtoul() nie chce działać więc musi być atoi() */
        /* sprawdź czy jest czas snu */
        else if(strcmp(argv[i], "-s") == 0 && i + 1 < argc && (temp = atoi(argv[i + 1])) > 0){
            i++;
            sleep_time = temp;
        }
        /* sprawdź czy jest mmap */
        else if(strcmp(argv[i], "-t") == 0 && i + 1 < argc && (temp = atoi(argv[i + 1])) > 0){
            i++;
            threshold = temp;
        }
        else{
            errno = EINVAL;
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

/* rekurencyjnie usuń katalog */
int remove_directory(char *dir_path){
    errno = 0;
    DIR *dir = opendir(dir_path);
    int result = -1;

    if(dir != NULL){
        struct dirent *entry;
        result = 0;
        char *buffer;

        /* odczytujemy rekord z katalogu i zapisujemy w entry*/
        while(result == 0 && (entry = readdir(dir))){
            int result2 = -1;

            /* jeśli entry zawiera . lub .. to przechodzimy do kolejnego rekordu */
            if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0){
                continue;
            }
            /* stwórz pełną ścieżkę do danego elementu*/
            size_t length = strlen(dir_path) + strlen(entry->d_name) + 2;
            buffer = malloc(length);
            if(buffer != NULL){
                struct stat statbuffer;

                /* zapisujemy do buffor ścieżkę pliku/katalogu reprezentowanego przez dany rekord */
                snprintf(buffer, length, "%s/%s", dir_path, entry->d_name);

                /* sprawdzamy czy plik/katalog o ścieżce określonej w buforze istnieje i zapisujemy dane o nim do statbuffer */ 
                if(stat(buffer, &statbuffer) == 0){
                    /* sprawdzamy czy ścieżka zawarta w buforze jest katalogiem - jeśli tak to rekurencyjnie wywołujemy na niej
                       naszą funkcję, w innym przypadku usuwamy */
                    if(S_ISDIR(statbuffer.st_mode)){
                        result2 = remove_directory(buffer);
                    }
                    else{
                        result2 = unlink(buffer);
                        syslog(LOG_INFO, "%s: File '%s' removed.\n", get_time_str(), buffer);
                    }
                }
                /* zwalniamy bufor przechowujący ścieżkę */
                free(buffer);
            }
            result = result2;
        }
        /* zamykamy strumień katalogu */
        closedir(dir);
    }

    /* usuwamy katalog */
    if(result == 0){
        result = rmdir(dir_path);
    }

    return result;
}

/* kopiuj plik */
int copy_file(char *src, char *dst){
    int size = 1024;
    int readFile, writeFile;
    /* Flagi:
       Create file if it does not exist | Open for writing only | Truncate flag */
    int flags = O_CREAT | O_WRONLY | O_TRUNC;
    /* Pozwolenia:
       User Read | User Write | Group Read | Group Write | Other Users Read | Other Users Write */
    int perms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;

    errno = 0;
    /* jeśli nie udało się stworzyć/otworzyć któregoś pliku to wychodzimy z funkcji */
    if((readFile = open(src, O_RDONLY)) == -1 || (writeFile = open(dst, flags, perms)) == -1){
        syslog(LOG_ERR, "%s: Could not access file '%s': %s.\n", get_time_str(), (readFile == -1)? src : dst, strerror(errno));
        close(readFile != -1 ? readFile : writeFile);
        return EXIT_FAILURE;
    }

    char *buffer = (char*)malloc(size);
    ssize_t content_size;
    /* pobierz rozmiar pliku źródłowego */
    int fileSize = lseek(readFile, 0, SEEK_END);
    lseek(readFile, 0, SEEK_SET);

    /* klasyczne kopiowanie przy pomocy read */
    if(threshold == 0 || fileSize < threshold){
        /* tak długo jak rozmiar zwróconego segmentu danych większy od 0 - skopiuj do nowej lokacji */
        while((content_size = read(readFile, buffer, size)) > 0)
        {
            write(writeFile, buffer, content_size);
        }
    }
    /* kopiowanie mmapem */
    else{
        void *mappedArea = mmap(NULL, fileSize, PROT_READ, MAP_PRIVATE, readFile, SEEK_SET);
        write(writeFile, mappedArea, fileSize);
        munmap(mappedArea, fileSize);
    }

    /* zamykamy oba strumienie i zwalniamy pamięć */
    close(readFile);
    close(writeFile);
    free(buffer);
    return EXIT_SUCCESS;
}

/* liczenie elementów katalogu */
int count_directory_entries(DIR *dir){
    int counter = 0;
    struct dirent *entry;
    while((entry = readdir(dir)) != NULL){
        /* nie liczymy . i ..*/
        if(strcmp("..", entry->d_name) < 0){
            counter++;   
        }
    }
    /* wracamy na początek katalogu */
    rewinddir(dir);
    return counter;
}

/* funkcja pomocnicza do sortowania zawartości katalogów - porównuje dwa structy dirent po nazwie */
int dirent_compare(const void *a, const void *b){
    return strcmp(((struct dirent*)a)->d_name, ((struct dirent*)b)->d_name);
}

/* sortowanie zawartości katalogów po nazwach */
int sort_directory(DIR *unsorted, struct dirent *sorted){
    struct dirent *entry;
    int length = 0;
    /* przenieś zawartość katalogu do nowej tablicy pomijając . i .. */
    while((entry = readdir(unsorted)) != NULL){
        if(strcmp("..", entry->d_name) < 0){
            sorted[length++] = *entry;
        }
    }
    rewinddir(unsorted);
    /* posortuj zawartość nowej tablicy quick-sortem */
    qsort(sorted, length, sizeof(struct dirent), dirent_compare);
    return EXIT_SUCCESS;
}

/* deklaracja funkcji dir_sync potrzebna do dwu-poziomowej rekurencji */
int sync_directories(char *src, char *dst);

/* usuwamy dany element katalogu */
int delete_element(char *dir_path, char *elementName){
    /* tworzymy ścieżkę do elementu */
    int result = EXIT_SUCCESS;
    struct stat statBuffer;
    int len = strlen(dir_path) + strlen(elementName) + 2;
    char *buffer = malloc(sizeof(char)*len);
    snprintf(buffer, len, "%s/%s", dir_path, elementName);
    /* pobieramy dane elementu który chcemy usunąć */
    if(stat(buffer, &statBuffer) == 0) {
        /* jeśli element jest katalogiem i opcja rekursywności aktywna to rekursywnie usuwamy */
        if(S_ISDIR(statBuffer.st_mode) && recursive && remove_directory(buffer) == 0){
            syslog(LOG_INFO,"%s: Directory '%s' successfully removed.\n", get_time_str(), buffer);
        }
        /* jeśli element nie jest katalogiem to usuwamy */
        else if (!S_ISDIR(statBuffer.st_mode) && unlink(buffer) == 0){
            syslog(LOG_INFO,"%s: File '%s' successfully removed.\n", get_time_str(), buffer);
        }
    }
    else{
        syslog(LOG_ERR, "%s: Could not access '%s'.\n", get_time_str(), buffer);
        result = EXIT_FAILURE;
    }
    free(buffer);
    return result;
}

/* synchronizujemy dany element katalogów */
int synchronize_element(char *src_dir_path, char *dst_dir_path, char *elementName){
    int result = EXIT_SUCCESS;
    /* potrzebne do zmiany czasu modyfikacji pliku */
    struct utimbuf timeTemp;
    /* structy do przachowywania danych o porównywanych plikach */
    struct stat srcStatBuffer, dstStatBuffer;
    /* tworzymy ścieżkę do elementu źródłowego */
    int len = strlen(src_dir_path) + strlen(elementName) + 2;
    char *srcBuffer = malloc(sizeof(char)*len);
    snprintf(srcBuffer, len, "%s/%s", src_dir_path, elementName);
    /* tworzymy ścieżkę do elementu docelowego */
    len = strlen(dst_dir_path) + strlen(elementName) + 2;
    char *dstBuffer = malloc(sizeof(char)*len);
    snprintf(dstBuffer, len, "%s/%s", dst_dir_path, elementName);
    if(stat(srcBuffer, &srcStatBuffer) == 0){
        /* próbujemy otworzyć element docelowy */
        int statResult = stat(dstBuffer, &dstStatBuffer);
        /* jeśli mamy do czynienia z katalogiem i rekursywność włączona to synchronizuj */
        if(recursive && S_ISDIR(srcStatBuffer.st_mode)){
            /* jeśli element docelowy nie istnieje to tworzymy */
            if( statResult != 0 && errno == ENOENT){
                mkdir(dstBuffer, srcStatBuffer.st_mode);
                statResult = stat(dstBuffer, &dstStatBuffer);
            }
            /* jeśli element docelowy istnieje ale nie jest katalogiem to usuwamy i tworzymy */
            else if(statResult == 0 && !S_ISDIR(dstStatBuffer.st_mode)){
                unlink(dstBuffer);
                mkdir(dstBuffer, srcStatBuffer.st_mode);
                statResult = stat(dstBuffer, &dstStatBuffer);
            }
            /* następnie prubujemy zsynchronizować oba katalogi i jeśli się powiodło umieszczamy informację w logu */
            if(statResult == 0 && sync_directories(srcBuffer, dstBuffer) == EXIT_SUCCESS){
                syslog(LOG_INFO,"%s: Directory '%s' synchronized.\n", get_time_str(), dstBuffer);
            }
        }
        /* jeśli element źródłowy nie jest katalogiem i daty modyfikacji różnią się to nadpisujemy */
        else if(!S_ISDIR(srcStatBuffer.st_mode)){
            /* jeśli element docelowy udało się otworzyć ale jest katalogiem to usuwamy */
            if(statResult == 0 && S_ISDIR(dstStatBuffer.st_mode)){
                remove_directory(dstBuffer);
                statResult = -1;
            }
            /* jeśli elementu docelowego nie udało się otworzyć lub jego data modyfikacji jest inna niż elementu źródłowego to kopiujemy */
            if(statResult != 0 || !(srcStatBuffer.st_mtime == dstStatBuffer.st_mtime)){ 
                copy_file(srcBuffer, dstBuffer);
                /* ustawiamy odpowiedni czas modyfikacji */
                timeTemp.actime = srcStatBuffer.st_atime;
                timeTemp.modtime = srcStatBuffer.st_mtime;
                utime(dstBuffer, &timeTemp);
                syslog(LOG_INFO,"%s: File '%s' successfully synchronized.\n", get_time_str(), dstBuffer);
            }
        }
    }
    else{
        syslog(LOG_ERR, "%s: Could not access '%s'.\n", get_time_str(), srcBuffer);
        result = EXIT_FAILURE;
    }
    free(srcBuffer);
    free(dstBuffer);
    return result;
}

/* synchronizuj podane katalogi */
int sync_directories(char *src, char *dst){
    errno = 0;
    /* otwieramy katalogi, w sytuacji jeśli nie udało się otworzyć któregoś
       katalogu, wychodzimy z funkcji */
    DIR *srcDir, *dstDir;
    if((srcDir = opendir(src)) == NULL || (dstDir = opendir(dst)) == NULL){
        if(srcDir != NULL) closedir(srcDir);
        syslog(LOG_ERR, "%s: Could not access directory '%s': %s\n", get_time_str(), (srcDir == NULL)? src : dst, strerror(errno));
        return EXIT_FAILURE;
    }

    /* gdy oba katalogi otworzone to liczymy ich elementy */
    int srcCount = count_directory_entries(srcDir), dstCount = count_directory_entries(dstDir);
    /* następnie alokujemy pamięć dla tablic przeznaczonych na posortowaną zawartość katalogów */
    struct dirent *srcEntries = malloc(sizeof(struct dirent) * srcCount), *dstEntries = malloc(sizeof(struct dirent) * dstCount);
    /* sortujemy zawartości katalogów po nazwach */
    sort_directory(srcDir, srcEntries); sort_directory(dstDir, dstEntries);
    /* zamykamy strumienie katalogów - od teraz używamy posortowanych tablic */
    closedir(srcDir); closedir(dstDir);
    /* iterujemy przez oba katalogi porównując ich zawartość do momentu gdy któryś zostanie w całości sprawdzony */
    int i = 0, j = 0;
    while(i < srcCount && j < dstCount){
        /* porównujemy nazwy plików */
        int comparison = strcmp(srcEntries[i].d_name, dstEntries[j].d_name);

        /* jeśli jakiś element istnieje tylko w katalogu docelowym to go usuwamy */
        if(comparison > 0){
            delete_element(dst, dstEntries[j].d_name);
            j++;
        }
        /* jeśli jakiś element istnieje tylko w katalogu źródłowym to dodajemy go do katalogu docelowego */
        else if(comparison < 0){
            synchronize_element(src, dst, srcEntries[i].d_name);
            i++;
        }
        /* jeśli jakiś element istnieje w obydwu katalogach to porównujemy ostatnią datę modyfikacji 
           i gdy się różnią to nadpisujem plik docelowy */
        else{
            synchronize_element(src, dst, srcEntries[i].d_name);
            i++;
            j++;
        }
    }

    /* jeśli pozostały jakieś elementy w katalogu źródłowym to kopiujemy je do docelowego */
    while(i < srcCount){
        synchronize_element(src, dst, srcEntries[i].d_name);
        i++;
    }

    /* jeśli pozostały jakieś elementy w katalogu docelowym to je usuwamy */
    while(j < dstCount){
        delete_element(dst, dstEntries[j].d_name);
        j++;
    }

    /* zwalniamy pamięć */
    free(srcEntries);
    free(dstEntries);
    return EXIT_SUCCESS;
}

/* funkcja obsługująca sygnał budzący demona - SIGUSR1 */
void sigusr1_handler(int sig){
    if(sig == SIGUSR1){
        syslog(LOG_INFO, "%s: SIGUSR1 handled, daemon awaken.\n", get_time_str());
        sync_directories(root_src, root_dst);
        free(current_time);
        syslog(LOG_INFO, "%s: Daemon goes back to sleep.\n", get_time_str());
    }
}

static void initialize_daemon(){
    /* Tworzenie nowego procesu który po terminacji
       rodzica będzie działał w tle*/
    pid_t pid = fork();
    /* Gdy nie powiodło się terminujemy aplikację */
    if(pid < 0){
        syslog(LOG_ERR,"Fork failed: %s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    /* Gdy powiodło się terminujemy rodzica
       (wartość zmiennej pid w przypadku procesu rodzica (który chcemy sterminować) będzie równa id procesu
       potomnego tymczasem wartość zmiennej pid w procesie potomnym jest równa 0) */
    if (pid > 0){
        exit(EXIT_SUCCESS);
    }

    /* Proces staje się liderem sesji */
    int sid = setsid();
    if(sid < 0){
        syslog(LOG_ERR, "Session failed: %m\n");
        exit(EXIT_FAILURE);
    }
    syslog(LOG_INFO, "Session Successfull.\n");

    /* Obsługujemy sygnał SIGUSR1 */
    struct sigaction sigusr1_action;
    sigusr1_action.sa_handler = sigusr1_handler;
    sigemptyset(&sigusr1_action.sa_mask);
    sigusr1_action.sa_flags = 0;
    sigaction(SIGUSR1, &sigusr1_action, NULL);
    syslog(LOG_INFO, "SIGUSR1 handler configured.\n");

    /* forkujemy po raz drugi */
    if((pid = fork()) < 0){
        syslog(LOG_ERR, "Fork failed: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if (pid > 0){
        exit(EXIT_SUCCESS);
    }
    syslog(LOG_INFO, "Fork Successfull.\n");

    /* ustawiamy poprawną maskę dostępu do plików
       aby móc poprawnie tworzyć i modyfikować pliki */
    umask(0);
    syslog(LOG_INFO, "Umask Done.\n");

    /* Ustawiamy katalog roboczy daemona na taki który na 
       pewno będzie istniał w systemie - dla linuxa jest to root */
    if(chdir("/") < 0){
        syslog(LOG_ERR, "Changing working directory to root failed: %m\n");
        exit(EXIT_FAILURE);
    }
    syslog(LOG_INFO, "Working directory changed.\n");

    /* zamykamy standardowe deskryptory IO - daemon i tak nie będzie korzystał z
       terminalu a stanowią one potencjalną lukę w bezpieczeństwie */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    syslog(LOG_INFO,"%s: Standard I/O closed. Daemon initialized.\n", get_time_str());
}

int main(int argc, char **argv){
    errno = 0;
    /* Weryfikacja poprawności dostarczonch argumentów */
    if(validate_arguments(argc, argv) == EXIT_FAILURE){
        perror("Use -h option to display help.\n");
        exit(EXIT_FAILURE);
    }
    /* pobieramy ścieżki absolutne do katalogów */
    realpath(argv[1], root_src);
    realpath(argv[2], root_dst);
    printf("Synchronization of following directories started:\n%s\n%s\nCheck syslog for more info.\n", root_src, root_dst);

    /* Otwieramy log systemowy */
    openlog("dirsync", LOG_PID, LOG_DAEMON);
    syslog(LOG_INFO, "Log  opened.\n");

    /* Funkcja inicjalizująca daemon */
    //initialize_daemon();

    /* Nieskończona pętla w której zawarta jest główna funkcjonalność daemona */
    while(1){
        sleep(sleep_time);
        syslog(LOG_INFO,"%s: Daemon awaken.\n", get_time_str());
        /* próbujemy zsynchronizować katalogi */
        if(sync_directories(root_src, root_dst) == EXIT_SUCCESS){
            syslog(LOG_INFO,"%s: Synchronization complete.\n", get_time_str());
        }
        else{
            syslog(LOG_ERR,"%s: Synchronization failed.\n", get_time_str());
        }
        syslog(LOG_INFO,"%s: Daemon goes back to sleep.\n", get_time_str());
        break;
    }
    free(current_time);
    return EXIT_SUCCESS;
}
